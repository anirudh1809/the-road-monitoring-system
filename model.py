import time
from dateutil import tz
from datetime import datetime
import gpxpy
import torch
import os
import pandas as pd

print(
    f"Setup complete. Using torch {torch.__version__} ({torch.cuda.get_device_properties(0).name if torch.cuda.is_available() else 'CPU'})")


def runModel(category):
    if category == "potholes":
        os.system(
            'python yolov5\detect.py --weights "GPL/PM.pt" --img 640 --conf 0.1 --source "input_video.mp4" ')
    else:
        os.system(
            'python yolov5\detect.py --weights "GPL/DM.pt" --img 640 --conf 0.1 --source "input_video.mp4" ')


def generate_media_list(top_folder_list, folder_name):
    file_list = []
    name_list = []
    for top_folder in top_folder_list:
        for root, dirs, files in os.walk(top_folder):
            file_list.extend([os.path.join(root, name) for name in files])

            name_list.extend([name for name in files])

    df_media = pd.DataFrame({'path': file_list, 'name': name_list})
    path_column = df_media.columns.get_loc('path')
    ctime_list = []
    mtime_list = []
    atime_list = []
    megabytes_list = []

    for i in range(len(df_media)):
        file_stats = os.stat(df_media.iloc[i, path_column])
        ctime_list.append(time.ctime(file_stats.st_ctime))

        mtime_list.append(time.ctime(file_stats.st_mtime))

        atime_list.append(time.ctime(file_stats.st_atime))
        megabytes_list.append(file_stats.st_size / 1000000)

    df_media['ctime'] = pd.to_datetime(ctime_list)
    df_media['modified_time'] = pd.to_datetime(mtime_list)
    df_media['accessed_time'] = pd.to_datetime(atime_list)
    df_media['megabytes'] = megabytes_list

    df_media['extension'] = df_media['name'].str.split('.').str[-1].str.lower()

    clip_extensions = ['mp4', 'mov', 'mts']
    pic_extensions = ['jpg', 'tiff', 'png', 'jpeg', 'gif']

    df_media['type'] = df_media['extension'].apply(
        lambda x: 'clip' if x in clip_extensions
        else 'pic' if x in pic_extensions else 'other')
    df_media.to_csv(f'{folder_name}_media_list.csv', index=False)
    return df_media


def retrieve_pic_locations(df_pics):
    photo_location_lat_list = []
    photo_location_lon_list = []
    alt_capture_time_list = []
    path_column = df_pics.columns.get_loc('path')
    for i in tqdm(range(len(df_pics))):

        try:
            with open(df_pics.iloc[i, path_column], 'rb') as image_file:
                current_image = Image(image_file)

            try:

                lat_tuple = current_image.gps_latitude
                lat_ref = current_image.gps_latitude_ref
                lon_tuple = current_image.gps_longitude
                lon_ref = current_image.gps_longitude_ref
                decimal_lat = lat_tuple[0] + \
                              lat_tuple[1] / 60 + lat_tuple[2] / 3600
                if lat_ref == 'S':
                    decimal_lat *= -1

                decimal_lon = lon_tuple[0] + \
                              lon_tuple[1] / 60 + lon_tuple[2] / 3600
                if lon_ref == 'W':
                    decimal_lon *= -1

                photo_location_lat_list.append(decimal_lat)
                photo_location_lon_list.append(decimal_lon)

            except:
                photo_location_lat_list.append(0)
                photo_location_lon_list.append(0)

            try:
                alt_capture_time_list.append(pd.to_datetime(
                    current_image.datetime.replace(':', '-', 2)))
            except:
                alt_capture_time_list.append('x')

        except:
            photo_location_lat_list.append(decimal_lat)
            photo_location_lon_list.append(decimal_lon)
            alt_capture_time_list.append('x')

    df_pics['raw_location'] = 0
    df_pics['lat'] = photo_location_lat_list
    df_pics['lat'] = pd.to_numeric(df_pics['lat'])
    df_pics['lon'] = photo_location_lon_list
    df_pics['lon'] = pd.to_numeric(df_pics['lon'])
    df_pics['alt_capture_time'] = alt_capture_time_list
    return df_pics


def retrieve_clip_locations(df_clips):
    video_location_list = []
    alt_capture_time_list = []
    path_column = df_clips.columns.get_loc('path')
    for i in tqdm(range(len(df_clips))):

        try:
            metadata = ffmpeg.probe(df_clips.iloc[i, path_column])

            try:

                if 'location' in metadata['format']['tags'].keys():
                    video_location_list.append(
                        metadata['format']['tags']['location'])
                elif 'com.apple.quicktime.location.ISO6709' in metadata[
                    'format']['tags'].keys():
                    video_location_list.append(
                        metadata['format']['tags'][
                            'com.apple.quicktime.location.ISO6709'])
                else:
                    video_location_list.append('xxxxxxxxxxxxxxxxx')

            except:
                video_location_list.append('xxxxxxxxxxxxxxxxx')

            try:
                if 'com.apple.quicktime.creationdate' in metadata[
                    'format']['tags'].keys():
                    alt_capture_time_list.append(
                        pd.to_datetime(metadata[
                                           'format']['tags']['com.apple.quicktime.creationdate']))
                else:
                    alt_capture_time_list.append('x')
            except:
                alt_capture_time_list.append('x')
        except:
            video_location_list.append('xxxxxxxxxxxxxxxxx')
            alt_capture_time_list.append('x')

    df_clips['alt_capture_time'] = alt_capture_time_list
    df_clips['raw_location'] = video_location_list

    df_clips['lat'] = df_clips['raw_location'].str[0:8]

    df_clips['lon'] = df_clips['raw_location'].str[8:17]

    df_clips['lat'] = df_clips['lat'].str.replace('xxxxxxxx', '0')
    df_clips['lon'] = df_clips['lon'].str.replace('xxxxxxxxx', '0')

    df_clips['lat'] = pd.to_numeric(df_clips['lat'])
    df_clips['lon'] = pd.to_numeric(df_clips['lon'])
    return df_clips


def generate_loc_list(df_media, folder_name):
    df_clips = df_media.query("type == 'clip'").copy()
    df_pics = df_media.query("type == 'pic'").copy()
    print("Retrieving picture locations:")
    df_pic_locs = retrieve_pic_locations(df_pics)
    print("Retrieving clip locations:")
    df_clip_locs = retrieve_clip_locations(df_clips)

    df_media_locs = pd.concat([df_pic_locs, df_clip_locs])
    df_media_locs.to_csv(f'{folder_name}_media_locations.csv', index=False)
    return df_media_locs


def map_media_locations(df_locations, file_name, folder_path=None,
                        add_paths=False, starting_location=[39, -95], zoom_start=4,
                        timestamp_column='modified_time', longitude_cutoff=80,
                        marker_type='CircleMarker', circle_marker_color='#ff0000', radius=5,
                        path_color='#3388ff', path_weight=3, tiles='Stamen Terrain'):
    m = folium.Map(location=starting_location, zoom_start=zoom_start,
                   tiles=tiles)
    locations_to_map = df_locations.query("lat != 0 & lon != 0").copy()
    # The above line removes any 'Null Island' geotags from the map.

    lat_column = locations_to_map.columns.get_loc('lat')
    lon_column = locations_to_map.columns.get_loc('lon')
    timestamp_column = locations_to_map.columns.get_loc(timestamp_column)
    file_path_column = locations_to_map.columns.get_loc('path')
    stroke_opacity = radius / 5  # If CircleMarkers will be used to show the
    # geotags, then the stroke value will be one fifth of the radius value.
    name_column = locations_to_map.columns.get_loc('name')

    marker_count = 0

    if add_paths == True:
        g = Geod(ellps="WGS84")

        for i in range(1, len(locations_to_map)):
            lat = locations_to_map.iloc[i, lat_column]
            lon = locations_to_map.iloc[i, lon_column]

            prev_lat = locations_to_map.iloc[
                i - 1, locations_to_map.columns.get_loc('lat')]
            prev_lon = locations_to_map.iloc[
                i - 1, locations_to_map.columns.get_loc('lon')]

            if (lat == prev_lat) & (lon == prev_lon):
                continue  # If this row's points are the same as the last row's,
                # there's no need to attempt to draw a line between them.
            if lon > longitude_cutoff:  # See above for explanation
                mapped_lon = lon - 360
            else:
                mapped_lon = lon

            if prev_lon > longitude_cutoff:
                mapped_prev_lon = prev_lon - 360
            else:
                mapped_prev_lon = prev_lon

            gc_points = g.npts(mapped_prev_lon, prev_lat,
                               mapped_lon, lat, 20, initial_idx=0, terminus_idx=0)

            flipped_gc_points = ([(coords[1], coords[0] - 360)
                                  if coords[0] > longitude_cutoff else (coords[1], coords[0])
                                  for coords in gc_points])

            folium.PolyLine(flipped_gc_points, color=path_color,
                            weight=path_weight).add_to(m)

    for i in range(len(locations_to_map)):
        lat = locations_to_map.iloc[i, lat_column]
        lon = locations_to_map.iloc[i, lon_column]
        if lon > longitude_cutoff:
            mapped_lon = lon - 360
        else:
            mapped_lon = lon
        timestamp = locations_to_map.iloc[i, timestamp_column]
        tooltip = str(timestamp) + ': ' + str(lat) + ', ' + str(lon)
        file_path = locations_to_map.iloc[i, file_path_column]
        modified_fp = file_path.replace('\\', '/')

        try:
            if marker_type == 'CircleMarker':
                folium.CircleMarker([lat, mapped_lon],
                                    color='#000000',
                                    radius=radius,
                                    weight=0.5,
                                    opacity=stroke_opacity,
                                    fill_color=circle_marker_color,
                                    fill_opacity=1.0,
                                    tooltip=tooltip,
                                    popup=modified_fp).add_to(m)
            else:
                folium.Marker([lat, mapped_lon],
                              tooltip=tooltip,
                              popup=modified_fp).add_to(m)

            marker_count += 1

        except:
            continue

    print("Added", marker_count, "markers to the map.")

    # Finally, the function will save the map in the location specified.
    if folder_path != None:
        m.save(f'{folder_path}/{file_name}_locations.html')
    else:
        m.save(f'{file_name}_locations.html')
    return m


def folder_list_to_map(top_folder_list, file_name, folder_path=None):
    df_media = generate_media_list(top_folder_list=top_folder_list,
                                   file_name=file_name)
    # generate_media_list saves the output as a .csv
    df_locations = generate_loc_list(df_media=df_media,
                                     file_name=file_name)
    # generate_loc_list also saves the output as a .csv
    location_map = map_media_locations(df_locations=df_locations,
                                       file_name=file_name, folder_path=folder_path)
    # map_media_locations saves the output as an .html file
    return (location_map)


def flip_lon(df, lat_south_bound, lat_north_bound,
             lon_west_bound, lon_east_bound):
    df['lon'] = df.apply(lambda row: row['lon'] * -1 if
    (row['lat'] > lat_south_bound) & (row['lat'] < lat_north_bound) &
    (row['lon'] > lon_west_bound) & (
            row['lon'] < lon_east_bound)
    else row['lon'], axis=1)
    return df


def create_map_screenshot(absolute_path_to_map_folder, map_name,
                          screenshot_save_path=None):
    ff_driver = webdriver.Firefox()

    window_width = 3000  # This produces a large window that can better
    # capture small details (such as zip code shapefiles).
    ff_driver.set_window_size(window_width, window_width * (9 / 16))  # Creates
    # a window with an HD/4K/8K aspect ratio
    ff_driver.get(f'{absolute_path_to_map_folder}\\{map_name}')
    # See https://www.selenium.dev/documentation/webdriver/browser/navigation/
    time.sleep(2)  # This gives the page sufficient
    # time to load the map tiles before the screenshot is taken.
    # You can also experiment with longer sleep times.

    if screenshot_save_path != None:
        # If specifying a screenshot save path, you must create this path
        # within your directory before the function is run; otherwise,
        # it won't return an image.
        ff_driver.get_screenshot_as_file(
            screenshot_save_path + '/' + map_name.replace('.html', '') + '.png')
    else:  # If no save path was specified for the screenshot, the image
        # will be saved within the project's root folder.
        ff_driver.get_screenshot_as_file(
            map_name.replace('.html', '') + '.png')
    # Based on:
    # https://www.selenium.dev/selenium/docs/api/java/org/openqa/selenium/TakesScreenshot.html

    ff_driver.quit()
    # Based on: https://www.selenium.dev/documentation/webdriver/browser/windows/


def batch_create_map_screenshots(absolute_path_to_map_folder,
                                 screenshot_save_path=None):
    for root, dirs, files in os.walk(absolute_path_to_map_folder):
        maps_list = files
    for map in maps_list:
        create_map_screenshot(absolute_path_to_map_folder,
                              map_name=map, screenshot_save_path=screenshot_save_path)


def convert_png_to_smaller_jpg(png_folder, png_image_name, jpg_folder,
                               reduction_factor=1, quality_factor=50):
    with PIL.Image.open(f'{png_folder}/{png_image_name}') as map_image:
        (width, height) = (map_image.width // reduction_factor,
                           map_image.height // reduction_factor)
        jpg_image = map_image.resize((width, height))

        jpg_image = jpg_image.convert('RGB')

        jpg_image_name = png_image_name.replace('png', 'jpg')
        jpg_image.save(f'{jpg_folder}/{jpg_image_name}',
                       format='JPEG', quality=quality_factor, optimize=True)
        # See https://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html#jpeg


def batch_convert_pngs_to_smaller_jpgs(png_folder, jpg_folder,
                                       reduction_factor, quality_factor):
    for root, dirs, files in os.walk(png_folder):
        png_image_list = files
    for png_image_name in png_image_list:
        convert_png_to_smaller_jpg(png_folder, png_image_name, jpg_folder,
                                   reduction_factor, quality_factor)


def calculate_distance_by_year(df_locations, unit='miles',
                               timestamp_column='modified_time'):
    df = df_locations.copy()
    df['distance'] = 0
    lat_col = df.columns.get_loc('lat')
    lon_col = df.columns.get_loc('lon')
    dist_col = df.columns.get_loc('distance')
    for i in range(1, len(df)):
        prev_pt = (df.iloc[i - 1, lat_col],
                   df.iloc[i - 1, lon_col])
        cur_pt = (df.iloc[i, lat_col],
                  df.iloc[i, lon_col])
        if unit == 'miles':
            df.iloc[i, dist_col] = haversine(prev_pt, cur_pt,
                                             unit=Unit.MILES)

        else:
            if unit != 'kilometers':
                print("This function supports miles and kilometers for units. \
Using kilometers as the distance measure.")
            df.iloc[i, dist_col] = haversine(prev_pt, cur_pt)  # Calculates

    df['year'] = df[timestamp_column].dt.to_period('Y')

    df_stats_by_year = df.pivot_table(index='year', values=[
        'distance', 'lat'], aggfunc={
        'distance': 'sum', 'lat': 'count'})

    df_stats_by_year.rename(columns={'lat': 'geotags',
                                     'distance': 'total_distance'}, inplace=True)
    df_stats_by_year.reset_index(inplace=True)
    return df_stats_by_year
