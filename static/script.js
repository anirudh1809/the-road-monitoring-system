//Taking inputs
function uploadData() {
    const fileInput = document.getElementById('inputGroupFile');
    const categorySelect = document.getElementById('inputGroupSelect');

    const formData = new FormData();
    formData.append('file', fileInput.files[0]);
    formData.append('category', categorySelect.value);

    fetch('/detect', {
        method: 'POST',
        body: formData
    })
        .then(response => response.text())
        .then(message => console.log(message))
        .catch(error => console.error(error));
}


// Graph Object
var barGraph, pieGraph;

function draw_graphs(xValues, yValues) {
    var graphColors = [
        '#007DF2',
        '#009AFA',
        '#0055F2',
        '#0070FA',
        '#0584E6',
        '#00B7FF',
        '#02D0F5'
    ];

    // Delete existing graphs
    if (window.barGraph !== undefined) {
        window.barGraph.destroy();
    }
    if (window.pieGraph !== undefined) {
        window.pieGraph.destroy();
    }

    barGraph = new Chart("barChart", {
        type: "bar",
        data: {
            labels: xValues,
            datasets: [{
                backgroundColor: graphColors,
                data: yValues
            }]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: "Detected count - Bar Graph"
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                    ticks: {
                        maxRotation: 0,
                        autoSkip: true,
                        maxTicksLimit: 2,
                        callback: function (value) {
                            // add line breaks to the label
                            var label = value.split(' ');
                            return [label[0], label[1] || ''].join('\n');
                        }
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    pieGraph = new Chart("pieChart", {
        type: "pie",
        data: {
            labels: xValues,
            datasets: [{
                backgroundColor: graphColors,
                data: yValues
            }]
        },
        options: {
            title: {
                display: true,
                text: "Detected Count - Pie Chart"
            },
            legend: {
                display: true,
                position: 'bottom'
            },
            responsive: true,
            maintainAspectRatio: false,
        }
    });
}

// Map Object
function generate_map() {
    var map = L.map('map').setView([17.686815, 83.218483], 13);
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
    }).addTo(map);
}

// Load pothole detection template
function load_data(data) {

    // Results - Section
    var myDiv = document.getElementById('result-section');
    myDiv.innerHTML = "";

    for (let x in data) {
        // Create a new h6 element
        var myH6 = document.createElement('h6');

        // Set the class of the h6 element
        myH6.className = 'col-3 mb-2 pb-2';

        // Set the text content of the h6 element
        myH6.textContent = x;

        // Create a new line break element
        var myBr = document.createElement('br');

        // Add the line break element to the h6 element
        myH6.appendChild(myBr);

        // Create a new span element
        var mySpan = document.createElement('span');

        // Set the class of the span element
        mySpan.className = 'fs-6 text-muted';

        // Set the text content of the span element
        mySpan.textContent = data[x];

        // Add the span element to the h6 element
        myH6.appendChild(mySpan);

        // Add the h6 element to the page
        myDiv.appendChild(myH6);
    }

    // Graph Section
    var xValues = Object.keys(data);
    var yValues = Object.values(data);
    xValues.shift()
    yValues.shift()
    draw_graphs(xValues, yValues);

}

// Select Change event to change the results and values
$('#select-task').on('change', function () {
    if (this.value == 1) {
        data = {
            "Total Count": 127,
            "Large / Severe Potholes": 54,
            "Medium / Intermediate Potholes": 41,
            "Small Potholes": 32
        };
        load_data(data);
    }
    else {
        data = {
            "Total Count": 127,
            "Potholes": 28,
            "Depression": 12,
            "Rutting": 17,
            "Raveling": 8,
            "Manhole": 32,
            "Road Traverse Cracks": 15,
            "Road Longitudinal Cracks": 15
        };
        load_data(data);
    }
});

// Run after load
window.onload = init;
function init() {
    // Generate map
    generate_map();

    // Set Sample graph
    data = {
        "Total Count": 127,
        "Large / Severe Potholes": 54,
        "Medium / Intermediate Potholes": 41,
        "Small Potholes": 32
    };
    load_data(data)

    // Load video
    var video_player = document.getElementById('video-player');
    video_player.src = "./static/sampleVideo.mp4"
}

function btnDetect() {
    // Get the Video document
    const fileInput = document.getElementById('inputGroupFile');
    const categorySelect = document.getElementById('select-task');

    const formData = new FormData();
    formData.append('category', categorySelect.value);
    formData.append('file', fileInput.files[0]);

    // Add loading animation
    var button = document.getElementById('btn-detect');
    var span = document.createElement("span");
    span.setAttribute("class", "spinner-border spinner-border-sm");
    span.setAttribute("role", "status");
    span.setAttribute("aria-hidden", "true");
    button.appendChild(span);

    fetch('/spatial', {
        method: 'POST',
        body: formData
    })
        .then(response => response.json())
        .then(data => {
            if (data.status === 'success'){
                // Load video

                // Load the info
                load_data(data.data);

                // Stop animation
                button.removeChild(span);
            }
            else{
                // Stop animation
                button.removeChild(span);
            }
        })
        .catch(error => console.error(error));
}