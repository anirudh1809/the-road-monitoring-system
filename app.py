from flask import Flask, render_template, url_for, redirect, flash, request, jsonify
from werkzeug.utils import secure_filename
from flask_cors import CORS
from model import runModel

users = {
    'road@garudalytics.com': 'password124',
    'test@garudalytics.com': 'password124'
}

app = Flask(__name__, static_url_path='/static')
CORS(app)
app.config['SECRET_KEY'] = 'VT05EXXKGQZXUIZX'


@app.route("/")
@app.route("/login", methods=['GET', 'POST'])
def login():
    print("Login Page active")
    return render_template('login_page.html', title='login')


# Login API
@app.route('/api/login', methods=['POST'])
def api_login():
    print('api is working')

    # Get login credentials from request
    username = request.form['username']
    password = request.form['password']

    # Check if credentials are valid
    if username in users and users[username] == password:
        response = {'status': 'success', 'message': 'Login successful'}
        return jsonify(response), 200
    else:
        response = {'status': 'error',
                    'message': 'Invalid username or password'}
        return jsonify(response), 401


@app.route("/spatial", methods=['POST', 'GET'])
def spatial():
    print("Spatial Page active")
    if request.method == 'POST':
        file = request.files['file']
        filename = secure_filename(file.filename)
        file.save("input_video.mp4")
        catergory = request.form['category']

        print('Video passed successfully')

        # Run model
        runModel(category=catergory)
        print("Model run successful")

        # Get the data
        data = get_counts("./static/counts.txt")
        return jsonify({'status': 'success', 'data': data})

    return render_template('spatial_page.html')


def get_counts(path):
    file = open(path, "r")
    f = file.read()
    f = f.split("\n")

    # Make dict of categories
    data = {}
    total = 0
    # data["Total"] = total
    for cat in f:
        dt = cat.split(" ")
        if len(dt) > 1:
            if dt[1] not in data:
                data[dt[1]] = int(dt[0])
            else:
                data[dt[1]] += int(dt[0])
            total += int(dt[0])
    # data["Total"] = total
    file.close()
    return data


if __name__ == '__main__':
    app.run(debug=True)
