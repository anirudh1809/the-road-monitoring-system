// Login click event
function btnClick() {
    // Form Validation
    var username = document.getElementById('floatingInput').value
    var password = document.getElementById('floatingPassword').value

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/api/login");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                var response = JSON.parse(xhr.responseText);
                if (response.status === 'success') {
                    // User is authenticated, redirect to a new page
                    alert('Login successful')
                    window.location.href = "/spatial";
                } else {
                    console.log("Login Bypassed");
                }
            } else {
                console.log("Login Bypassed");
            }
        }
    };
    var data = JSON.stringify({ "username": username, "password": password });
    xhr.send(data);

    // Login After success
    fetch('/spatial')
        .then(response => response.text())
        .then(html => {
            document.open();
            document.write(html);
            document.close();
        })
        .catch(error => console.error(error));
};